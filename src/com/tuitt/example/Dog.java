package com.tuitt.example;

// Child class of Animal
    // "extend" keyword is used to inherit the properties and methods of the class
public class Dog extends Animal{
    private String breed;

    public Dog(){
        super(); // direct access with thhahahahe origin constructor //accessing empty constructor
        this.breed = "Chihuahua";
    }

    public Dog(String name, String color, String breed){
        super(name, color);
        this.breed = breed;
    }

    public String getBreed(){
        return this.breed;
    }
    public void setBreed(String breed){
        this.breed = getBreed();
    }

    public void speak(){
        System.out.println("Woof woof!");
    }

    public void call(){
        // super.call(); // direct access with the parent method
        System.out.println("Hi my name is "+this.name + ",  I am a dog");
    }
}
