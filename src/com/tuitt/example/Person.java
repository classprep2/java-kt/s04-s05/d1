package com.tuitt.example;

public class Person implements Actions, Greetings {


    @Override
    public void sleep() {
        System.out.println("Zzzzzzzzz....");
    }

    @Override
    public void run() {
        System.out.print("Running");
    }

    public void morningGreet(){
        System.out.println("Good morning!");
    }
    public void holidayGreet() {
        System.out.println("Happy Holidays!");
    }
}
