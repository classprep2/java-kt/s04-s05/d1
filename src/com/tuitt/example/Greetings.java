package com.tuitt.example;

public interface Greetings {

    public void morningGreet();
    public void holidayGreet();
}
