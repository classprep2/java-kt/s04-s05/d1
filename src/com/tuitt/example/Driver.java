package com.tuitt.example;

public class Driver {
    //properties
    private String name;

    // empty constructor
    public Driver(){}

    // parameterized constructor
    public Driver(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }
}
