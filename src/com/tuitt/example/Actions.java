package com.tuitt.example;

public interface Actions {
    public void sleep();
    public void run();
}
