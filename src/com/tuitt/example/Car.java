package com.tuitt.example;

public class Car {
    // Access Modifier
    // These are used to restrict the scope of a class, constructor, variable, method or data member.
    // Four Types of Access Modifiers:
    //1. Default - No Keyword indicated (accessibility is within the package)
    //2. Private - Properties or method only accessible within the class.
    //3. Protected - Properties and methods are only accessible by the class of the same package and the subclass present in any package.
    //4. Public - Properties and methods can be accessed from anywhere.

    // Class creation
    // Four parts of class creation
    //1. Properties - characteristics of an object
    private String name;
    private String brand;
    private int yearofMake;

    // Make a component of a car
    private Driver driver;
    // Driver component under Driver class

    // 2. Constructor - used to create/instantiate an object
    // a. empty constructor - creates object that doesn't have any arguments/parameters.
    public Car(){ // default constructor\
        this.yearofMake = 2000;
        // b. parameterized constructor - creates an object with arguments/parameters
        this.driver = new Driver("Alejandro");

    }

    public Car(String name, String brand, int yearOfMake){
        this.name = name;
        this.brand = brand;
        this.yearofMake = yearOfMake;


    }

    // 3. Getters and setters - get and set the value of each property of the object

    // Getters - retrieve the value of instantiated object
    public String getName(){
        return this.name;
    }
    public String getBrand(){
        return this.brand;
    }
    public int getYearofMake() {
        return this.yearofMake;
    }
    public String getDriverName(){
        return this.driver.getName();
    }


    // Setter - use to change the default value of  an instantiated object
    public void setName(String name){
        this.name = name;
    }
    public void setBrand(String brand){
        this.brand = brand;
    }
    public void setYearofMake(int yearofMake){
        // can also be modified to add data validation.

        if(yearofMake < 2022){
            this.yearofMake = yearofMake;
        }
    }

    public void setDriver(String driver){
        this.driver.setName(driver);
    }
    // 4. Methods - functions an object can perform (action).
    public void drive(){
        System.out.println("The car is running. Vroom Vroom");
    }


}
